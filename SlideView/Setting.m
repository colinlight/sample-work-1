//
//  Setting.m
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "Setting.h"

@implementation Setting

@synthesize title;

@synthesize icon;

@synthesize subtitle;

-(void)dealloc
{
    [title release];
    [icon release];
    [subtitle release];
    [super dealloc];
}

@end
