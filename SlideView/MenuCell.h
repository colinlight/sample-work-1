//
//  SettingCell.h
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+SettingsCellStyles.h"

@class Setting;

@interface MenuCell : UITableViewCell
{
    Setting *setting;
}

@property (nonatomic, retain ) Setting *setting;

-(void)setSetting:(Setting *)newSetting withStyle:(SettingCellStyle)style;

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
        withDivider:(BOOL)divider;

@end
