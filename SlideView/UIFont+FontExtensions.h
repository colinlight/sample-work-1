//
//  UIFont+FontExtensions.h
//  owzit
//
//  Created by Colin Light on 24/10/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (FontExtensions)

+(UIFont*)getDefaultCellFontForSize:(int)fontSize;

+(UIFont*)getStandardCellSystemFont:(int)fontSize;

+(UIFont*)getStandardCellSystemFontNormal:(int)fontSize;

+(UIFont*)getOswaldCellFontForSize:(int)fontSize;

@end
