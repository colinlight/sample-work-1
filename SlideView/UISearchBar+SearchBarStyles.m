//
//  UISearchBar+SearchBarStyles.m
//  owzit
//
//  Created by Colin Light on 01/11/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "UISearchBar+SearchBarStyles.h"
#import "UIColor+ColorExtension.h"

@implementation UISearchBar (SearchBarStyles)

-(void)setDefaultBarStyle
{
    
    [self setBackgroundImage:[UIImage imageNamed:@"cell-background"]];
    [self setPlaceholder:@"Search"];
    
    [self setSearchFieldBackgroundImage:[UIImage imageNamed:@"search-bar-background"]
                                 forState:UIControlStateNormal];
    
    [self setImage:[UIImage imageNamed:@"icon-search-bar"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    self.tintColor = [UIColor blackColor];
    
    UITextField *searchField = [self valueForKey:@"_searchField"];
    [searchField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    searchField.textColor = [UIColor whiteColor];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self setShowsCancelButton:NO animated:YES];
    return YES;
}


@end
