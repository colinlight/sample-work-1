//
//  UITableView+SettingsTableStyles.m
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "UITableView+SettingsTableStyles.h"

@implementation UITableView (SettingsTableStyles)


-(void)setSettingsTableBackground
{
    UIView *back = [[UIView alloc]initWithFrame:self.frame];
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"settings-table-bg"]];;
    self.backgroundView = back;
    [back release];
}

@end
