//
//  AppDelegate.h
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideViewController.h"
#import "CustomTabController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CustomTabControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic ) SlideViewController *slideViewController;

@end
