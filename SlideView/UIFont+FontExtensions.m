//
//  UIFont+FontExtensions.m
//  owzit
//
//  Created by Colin Light on 24/10/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "UIFont+FontExtensions.h"

@implementation UIFont (FontExtensions)

+(UIFont*)getDefaultCellFontForSize:(int)fontSize
{
    return [UIFont fontWithName:@"RopaSans-Regular" size:fontSize];
}

+(UIFont*)getStandardCellSystemFont:(int)fontSize
{
    return [UIFont boldSystemFontOfSize:fontSize];
}

+(UIFont*)getStandardCellSystemFontNormal:(int)fontSize
{
    return [UIFont systemFontOfSize:fontSize];
}

+(UIFont*)getOswaldCellFontForSize:(int)fontSize
{
    return [UIFont fontWithName:@"Oswald" size:fontSize];
}


@end
