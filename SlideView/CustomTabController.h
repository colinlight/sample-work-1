//
//  EstablishmentsViewController.h
//  owzit
//
//  Created by Colin Light on 28/09/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTabBar.h"

@protocol CustomTabControllerDelegate <NSObject>

@required
-(void)setNavItemTitleForSection:(UINavigationItem*)item;

-(UIViewController*)getViewControllerForIndex:(int)index
                 withNavigationItem:(UINavigationItem*)item;

@end

@interface CustomTabController : UIViewController<CustomTabDelegate>
{
    UIViewController *subViewController;
    
    CustomTabBar *tabBar;
    
    UIImageView *tabShadow;
    
    UIImageView *divImage;
    
    UIActivityIndicatorView *loadIndicator;
    
    NSMutableDictionary *cachedViewControllers;
    
    NSMutableDictionary *dataModel;
    
    id<CustomTabControllerDelegate> delegate;
    
    int mTabIndex;
}

@property ( nonatomic, retain ) UIViewController *subViewController;

@property ( nonatomic, retain ) CustomTabBar *tabBar;

@property ( nonatomic, retain ) UIImageView *tabShadow;

@property ( nonatomic, retain ) UIImageView *divImage;

@property ( nonatomic, retain ) UIActivityIndicatorView *loadIndicator;

@property ( nonatomic, retain ) NSMutableDictionary *cachedViewControllers;

@property ( nonatomic, retain ) NSMutableDictionary *dataModel;

@property ( nonatomic, assign ) id<CustomTabControllerDelegate> delegate;

-(id)initWithNumberOfTabs:(int)numberOfTabs;

-(id)initWithNumberOfTabs:(int)numberOfTabs shouldDisplayTabBar:(BOOL)withTabBar shouldShowShadow:(BOOL)showShadow;

-(void)addTabForIndex:(int)index
     withDefaultImage:(UIImage*)defaultImage
    withSelectedImage:(UIImage*)selectedImage
        withIconImage:(UIImage*)icon
    withSelectedImage:(UIImage*)selectedIcon
            withTitle:(NSString*)tabTitle
shouldCacheViewController:(BOOL)shouldCache
     shouldHideShadow:(BOOL)shouldHide;

-(void)showActivityIndicatorAtPosY:(int)posy;

-(void)hideActivityIndicator;



@end
