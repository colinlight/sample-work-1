//
//  CustomTabBar.h
//  owzit
//
//  Created by Colin Light on 23/10/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomTabDelegate <NSObject>

-(void)selectedIndexChanged:(int)newIndex;

@end

@interface CustomTabBar : UIView
{
    NSMutableArray *tabs;
    int currentIndex;
    id<CustomTabDelegate> delegate;
}

@property ( nonatomic, retain ) NSMutableArray *tabs;

@property ( nonatomic, assign ) int currentIndex;

@property ( nonatomic, assign ) id<CustomTabDelegate> delegate;

- (id)initWithFrame:(CGRect)frame withNumberOfTabs:(int)number;

-(void)setTabSkinAtIndex:(int)index
        withDefaultImage:(UIImage*)defaultImg
                withSelectedImage:(UIImage*)selectedImg;

-(void)setTabTitleAtIndex:(int)index withString:(NSString*)tabLabel;

-(void)setTabIconAtIndex:(int)index
           withIconImage:(UIImage*)iconImg
       withSelectedImage:(UIImage*)selectedIcon;

-(void)setCurrentIndex:(int)ind;

-(IBAction)didSelectTab:(id)sender;

@end
