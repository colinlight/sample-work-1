//
//  BaseDataVO.m
//  owzit
//
//  Created by Colin Light on 28/09/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "BaseDataVO.h"

@implementation BaseDataVO


//maps a dictionary object properties to properties on this object
//typically you would extend this object to get this functionlity
-(void)mapToDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys])
    {
        if ([self respondsToSelector:NSSelectorFromString(key)])
            [self setValue:[dict objectForKey:key] forKey:key];
    }
}


@end
