//
//  HomeViewController.h
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideViewController.h"

@interface HomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *testTable;
    id<SlideViewDelegate> slideDelegate;
}

@property ( nonatomic, retain ) UITableView *testTable;

@property ( nonatomic, assign ) id<SlideViewDelegate> slideDelegate;

-(IBAction)settingsSelected:(id)sender;



@end
