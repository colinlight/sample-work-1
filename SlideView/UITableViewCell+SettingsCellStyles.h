//
//  UITableViewCell+SettingsCellStyles.h
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (SettingsCellStyles)

typedef NS_ENUM(NSInteger, SettingCellStyle){
    SettingCellStyleTop,
    SettingCellStyleMiddle,
    SettingCellStyleBottom,
};

-(void)setUserCellStyle;

-(void)setSettingCellWithStyle:(SettingCellStyle)style;

-(void)addDivider;

-(NSString*)getCellBackgroundNameForStyle:(SettingCellStyle)style;

@end


