//
//  SettingCell.m
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "MenuCell.h"
#import "Setting.h"

@interface MenuCell ()
-(void)setViewFrameX:(int)xPos forView:(UIView*)targetView;
@end

@implementation MenuCell

@synthesize setting;

static int DIVIDER_X_POS = 38;

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
        withDivider:(BOOL)divider
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addDivider];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setViewFrameX:45 forView:self.textLabel];
    [self setViewFrameX:45 forView:self.detailTextLabel];
    
    int xPos = DIVIDER_X_POS/2 - self.imageView.frame.size.width/2;
    [self setViewFrameX:xPos forView:self.imageView];
}

-(void)setSetting:(Setting *)newSetting withStyle:(SettingCellStyle)style
{
    [newSetting retain];
    [setting release];
    setting = newSetting;
    
    [self setSettingCellWithStyle:style];
    
    self.textLabel.text = setting.title;
    
    if ( setting.icon )
        self.imageView.image = [UIImage imageNamed:setting.icon];
    
    if ( setting.subtitle )
        self.detailTextLabel.text = setting.subtitle;
    
}


-(void)dealloc
{
    [setting release];
    [super dealloc];
}


#pragma mark - private methods
-(void)setViewFrameX:(int)xPos forView:(UIView*)targetView
{
    CGRect targetFrame = targetView.frame;
    targetFrame.origin.x = xPos;
    targetView.frame = targetFrame;;
}

@end
