//
//  DataModelParser.h
//  owzit
//
//  Created by Colin Light on 09/11/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kProfileKey @"profile"

#define kStreamKey @"stream"

#define kPlacesHeader @"placesHeader"

#define kPlacesModel @"placesModel"

#define kSettingsFollowKey @"canFollow"

//@class Establishment;

@class Settings;

@interface DataModelParser : NSObject

+(Settings*)parseSettingsJSON:(NSString*)json;

+(NSMutableArray*)parseSetting:(NSArray*)data;

//+(NSMutableArray*)parseStreamJSON:(NSString*)json;
//
//+(NSMutableArray*)parseStreamWithJSONArray:(NSArray*)streamData;
//
//+(NSMutableArray*)parseSectorJSON:(NSString*)json;
//
//+(NSMutableArray*)parseEstablishmentsJSON:(NSString*)json;
//
//+(Establishment*)parseSingleEstablishmentJSON:(NSString*)json;
//
//+(NSArray*)parseCountryJSON:(NSString*)json;
//
//+(NSMutableArray*)parseCommentJSON:(NSString*)json;
//
//+(NSDictionary*)parseProfileJSON:(NSString*)json;
//
//+(NSMutableArray*)parseFollowJSON:(NSString*)json;
//
//+(void)parseInformationJSON:(NSString *)json forEstablishemnt:(Establishment*)establishment;
//
//+(NSMutableArray*)parseReviewJSON:(NSString*)json;
//
//+(NSDictionary*)parseMyPlacesJSON:(NSString*)json;
//
//+(NSDictionary*)parseSettingsJSON:(NSString*)json;
//
//+(NSMutableArray*)parseProfileSearchJSON:(NSString*)json;
//
//+(NSDictionary*)parseReviewPostResultJSON:(NSString*)json;


@end
