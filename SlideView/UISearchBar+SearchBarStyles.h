//
//  UISearchBar+SearchBarStyles.h
//  owzit
//
//  Created by Colin Light on 01/11/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (SearchBarStyles)<UITextFieldDelegate>

-(void)setDefaultBarStyle;

@end
