//
//  EstablishmentsViewController.m
//  owzit
//
//  Created by Colin Light on 28/09/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "CustomTabController.h"
#import "CustomTabBar.h"

@interface CustomTabController ()

@end

@implementation CustomTabController

@synthesize subViewController;

@synthesize tabBar;

@synthesize tabShadow;

@synthesize divImage;

@synthesize loadIndicator;

@synthesize cachedViewControllers;

@synthesize dataModel;

@synthesize delegate;


static NSString *SHOULD_CACHE_KEY = @"cache";

static NSString *CACHED_CONTROLLER_KEY = @"controller";

static NSString *SHOULD_SHOW_SHADOW = @"showShadow";


#pragma mark - life cycle

-(id)initWithNumberOfTabs:(int)numberOfTabs shouldDisplayTabBar:(BOOL)withTabBar shouldShowShadow:(BOOL)showShadow
{
    self = [self initWithNumberOfTabs:numberOfTabs];
    if ( self ){
        
        CustomTabBar *customTabBar = [[CustomTabBar alloc]
                                      initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 61)
                                      withNumberOfTabs:numberOfTabs];
        
        [self.view addSubview:customTabBar];
        customTabBar.delegate = self;
        self.tabBar = customTabBar;
        [customTabBar release];
        
        
        UIImageView *shadow = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cell-grad"]];
        shadow.frame = CGRectMake(0, self.tabBar.frame.size.height, self.view.frame.size.width, 16);
        [self.view addSubview:shadow];
        self.tabShadow = shadow;
        [tabShadow release];
        
        UIImageView *dividerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"divider"]];
        CGRect divFrame = dividerImage.frame;
        divFrame.origin.y = self.tabBar.frame.size.height;
        dividerImage.frame = divFrame;
        [self.view addSubview:dividerImage];
        self.divImage = dividerImage;
        [dividerImage release];
        
    }
    return self;
}

-(id)initWithNumberOfTabs:(int)numberOfTabs
{
    self = [self init];
    if (self) {
    
        NSMutableDictionary *controllerCache = [[NSMutableDictionary alloc]init];
        self.cachedViewControllers = controllerCache;
        [controllerCache release];
        
        NSMutableDictionary *model = [[NSMutableDictionary alloc]init];
        self.dataModel = model;
        [model release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.delegate setNavItemTitleForSection:self.navigationItem];
    self.navigationController.navigationBarHidden = NO;
    
    if ( mTabIndex )
        return;
    
    [self.tabBar setCurrentIndex:0];
    [self selectedIndexChanged:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.subViewController = nil;
    self.tabBar = nil;
    self.tabShadow = nil;
    self.divImage = nil;
    self.loadIndicator = nil;
    self.cachedViewControllers = nil;
    self.dataModel = nil;
}

-(void)dealloc
{
    [subViewController release];
    [tabBar release];
    [tabShadow release];
    [divImage release];
    [loadIndicator release];
    [cachedViewControllers release];
    [dataModel release];
    [super dealloc];
}

#pragma mark - Public Methods
-(void)addTabForIndex:(int)index
     withDefaultImage:(UIImage*)defaultImage
    withSelectedImage:(UIImage*)selectedImage
        withIconImage:(UIImage*)icon
    withSelectedImage:(UIImage*)selectedIcon
            withTitle:(NSString*)tabTitle
shouldCacheViewController:(BOOL)shouldCache
     shouldHideShadow:(BOOL)shouldHide;
{
    [self.tabBar setTabSkinAtIndex:index
                   withDefaultImage:defaultImage
                  withSelectedImage:selectedImage];
    
    [self.tabBar setTabTitleAtIndex:index withString:tabTitle];
    
    [self.tabBar setTabIconAtIndex:index withIconImage:icon withSelectedImage:selectedIcon];
    
    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:shouldCache], SHOULD_CACHE_KEY,
                                   [NSNumber numberWithBool:shouldHide], SHOULD_SHOW_SHADOW, nil];
    
    [self.dataModel setObject:tempDict forKey:[NSNumber numberWithInt:index]];
    
    [self.tabBar layoutSubviews];
    
}


-(void)showActivityIndicatorAtPosY:(int)posy
{
    if ( !loadIndicator )
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]init];
        indicator.center = self.view.center;
        CGRect indicatorFrame = indicator.frame;
        indicatorFrame.origin.y = posy;
        indicator.frame = indicatorFrame;
        [self.view addSubview:indicator];
        self.loadIndicator = indicator;
        [indicator release];
    }
    
    [self.loadIndicator setHidden:NO];
    [self.loadIndicator startAnimating];

}

-(void)hideActivityIndicator
{
    [self.loadIndicator setHidden:YES];
    [self.loadIndicator stopAnimating];
}




#pragma mark - CustomTabDelegate methods
-(void)selectedIndexChanged:(int)newIndex
{

    if ( self.subViewController )
    {
        [self.subViewController.view removeFromSuperview];
        [self.subViewController removeFromParentViewController];
        self.subViewController = nil;
        [subViewController release];
    }
    
    UIViewController *nextViewController;
    
    NSDictionary *dataForIndex = [self.dataModel objectForKey:[NSNumber numberWithInt:newIndex]];

    NSNumber *show = [dataForIndex objectForKey:SHOULD_SHOW_SHADOW];
    bool hideTabShadow = ( show.intValue == 1 )? YES : NO;

    NSNumber *cached = [dataForIndex objectForKey:SHOULD_CACHE_KEY];
    if ( cached.intValue == 1 )
    {
        nextViewController = [self.cachedViewControllers
                                              objectForKey:[NSNumber numberWithInt:newIndex]];
        if ( !nextViewController )
        {
            nextViewController = [self.delegate getViewControllerForIndex:newIndex
                                                       withNavigationItem:self.navigationItem];
            
            [self.cachedViewControllers setObject:nextViewController
                                           forKey:[NSNumber numberWithInt:newIndex]];
        }
    }
    else
    {
        nextViewController = [self.delegate getViewControllerForIndex:newIndex
                                                   withNavigationItem:self.navigationItem];
    }
    
    if ( !nextViewController )
        return;
    
    self.subViewController = nextViewController;

    [self addChildViewController: self.subViewController];
    [self.view addSubview: self.subViewController.view];
    CGRect currentFrame = [self.view frame];
    
    self.subViewController.view.frame = ( self.tabBar )?
                CGRectMake(0, 61, currentFrame.size.width, currentFrame.size.height - 61) :
                CGRectMake(0, 0, currentFrame.size.width, currentFrame.size.height);
    
    [self.tabShadow setHidden:hideTabShadow];
    [self.view bringSubviewToFront:self.tabShadow];
    [self.view bringSubviewToFront:self.divImage];
    
    mTabIndex = newIndex;
}



@end
