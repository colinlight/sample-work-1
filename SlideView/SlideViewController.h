//
//  SlideViewController.h
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SlideViewDelegate <NSObject>

-(void)toggleSlideView;

@end

@interface SlideViewController : UIViewController<SlideViewDelegate>
{
    UIViewController *bottomViewController;
    UIViewController *topViewController;
    bool mIsClosed;
    CGPoint mStartPosition;
}

@property ( nonatomic, retain ) UIViewController *bottomViewController;

@property ( nonatomic, retain ) UIViewController *topViewController;

-(IBAction)userDidPan:(UIPanGestureRecognizer*)sender;

@end
