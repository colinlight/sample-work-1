//
//  main.m
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
