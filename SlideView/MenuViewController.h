//
//  SettingsViewController.h
//  SlideView
//
//  Created by Colin Light on 07/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTabController.h"
#import "Settings.h"


@interface MenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    UITableView *settingsTable;
    UISearchBar *searchBar;
    id<CustomTabDelegate> tabDelegate;
    Settings *settings;
}

@property ( nonatomic, retain ) IBOutlet UITableView *settingsTable;

@property ( nonatomic, retain ) IBOutlet UISearchBar *searchBar;

@property ( nonatomic, assign ) id<CustomTabDelegate> tabDelegate;

@property ( nonatomic, retain ) Settings *settings;

@end
