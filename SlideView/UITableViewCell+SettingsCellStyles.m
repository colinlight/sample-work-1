//
//  UITableViewCell+SettingsCellStyles.m
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "UITableViewCell+SettingsCellStyles.h"

@implementation UITableViewCell (SettingsCellStyles)

static int DIVIDER_X_POS = 37;

-(void)setUserCellStyle
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textLabel.textColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings-table-header"]];
    self.backgroundView = backgroundImage;
    self.contentView.backgroundColor = [UIColor clearColor];
    [backgroundImage release];
}


-(void)setSettingCellWithStyle:(SettingCellStyle)style;
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textLabel.textColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:
                                    [UIImage imageNamed:[self getCellBackgroundNameForStyle:style]]];
    self.backgroundView = backgroundImage;
    self.contentView.backgroundColor = [UIColor clearColor];
    [backgroundImage release];
}



-(void)addDivider
{
    UIImageView *dividerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings-cell-divider"]];
    dividerImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
    
    CGRect divRect = dividerImage.frame;
    divRect.origin.x =  DIVIDER_X_POS;
    divRect.size.height = self.frame.size.height;
    dividerImage.frame = divRect;
    
    [self.contentView addSubview:dividerImage];
    [dividerImage release];
}



-(NSString*)getCellBackgroundNameForStyle:(SettingCellStyle)style
{
    if ( style == SettingCellStyleTop )
         return @"settings-cell-top-bg";
    
    if ( style == SettingCellStyleBottom )
         return @"settings-cell-bottom-bg";
    
    return @"settings-cell-middle-bg";
}

@end
