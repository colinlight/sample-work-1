//
//  DataModelParser.m
//  owzit
//
//  Created by Colin Light on 09/11/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "DataModelParser.h"
#import "JSONKit.h"
#import "Setting.h"
#import "Settings.h"
//#import "Establishment.h"
//#import "Sector.h"
//#import "Review.h"
//#import "Comment.h"
//#import "Profile.h"
//#import "User.h"
//#import "Information.h"

@implementation DataModelParser


+(Settings*)parseSettingsJSON:(NSString*)json
{
    NSDictionary *model = [json objectFromJSONString];
    NSDictionary *menuTypes = [model objectForKey:@"types"];
    
    Settings *settings = [[Settings alloc]init];
    settings.sections = [DataModelParser parseSetting:[menuTypes objectForKey:@"settings"]];
    settings.account = [DataModelParser parseSetting:[menuTypes objectForKey:@"account"]];
    settings.options = [DataModelParser parseSetting:[menuTypes objectForKey:@"options"]];

    return [settings autorelease];
}

+(NSMutableArray*)parseSetting:(NSArray*)data
{
    NSMutableArray *settingList = [[NSMutableArray alloc]init];
    for (NSDictionary *dataItem in data)
    {
        Setting *setting = [[Setting alloc] init];
        [setting mapToDictionary:dataItem];
        [settingList addObject:setting];
        [setting release];
    }
    return [settingList autorelease];
}

//+(NSMutableArray*)parseStreamJSON:(NSString*)json
//{
//    NSDictionary *data = [json objectFromJSONString];
//    NSDictionary *sectorData = [data objectForKey:@"results" ];
//    NSArray *streamData = [sectorData objectForKey:@"stream"];
//    return [DataModelParser parseStreamWithJSONArray:streamData];
//}
//
//
//+(NSMutableArray*)parseStreamWithJSONArray:(NSDictionary*)streamData
//{
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *dataItem in streamData)
//    {
//        Review *review = [[Review alloc] init];
//        [review mapToDictionary:dataItem];
//        [model addObject:review];
//        [review release];
//    }
//    return [model autorelease];
//}
//
//+(NSMutableArray*)parseSectorJSON:(NSString*)json
//{
//    NSDictionary *resultData = [json objectFromJSONString];
//    NSArray *sectorList = [resultData objectForKey:@"results"];
//    
//    NSMutableArray *model= [[NSMutableArray alloc]init];
//    
//    for (NSDictionary *data in sectorList)
//    {
//        Sector *sector = [[Sector alloc]init];
//        sector.active = 1;
//        [sector mapToDictionary:data];
//        [model addObject:sector];
//        [sector release];
//    }
//    
//    return [model autorelease];
//}
//
//+(NSMutableArray*)parseEstablishmentsJSON:(NSString*)json
//{
//    NSDictionary *resultData = [json objectFromJSONString];
//    NSArray *establishmentData = [resultData objectForKey:@"results"];
//    
//    if ( [establishmentData count] <= 0 )
//        return nil;
//    
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *data in establishmentData)
//    {
//        Establishment *establishment = [[Establishment alloc] init];
//        [establishment mapToDictionary:data];
//        [model addObject:establishment];
//        [establishment release];
//    }
//    return [model autorelease];
//}
//
//+(Establishment*)parseSingleEstablishmentJSON:(NSString*)json
//{
//    NSDictionary *resultData = [json objectFromJSONString];
//    NSDictionary *establishmentData = [resultData objectForKey:@"result"];
//    
//    Establishment *establishment = [[Establishment alloc] init];
//    [establishment mapToDictionary:establishmentData];
//    return [establishment autorelease];
//}
//
//
//+(NSArray*)parseCountryJSON:(NSString*)json
//{
//    NSDictionary *resultsData = [json objectFromJSONString];
//    return [resultsData objectForKey:@"results" ];
//}
//
//+(NSMutableArray*)parseCommentJSON:(NSString*)json
//{
//    NSDictionary *parsedData = [json objectFromJSONString];
//    NSArray *resultsData = [parsedData objectForKey:@"results"];
//    
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *data in resultsData)
//    {
//        Comment *comment = [[Comment alloc]init];
//        [comment mapToDictionary:data];
//        [model addObject:comment];
//        [comment release];
//    }
//    
//    return [model autorelease];
//}
//
//
//+(NSDictionary*)parseProfileJSON:(NSString *)json
//{
//    NSDictionary *data = [json objectFromJSONString];
//    NSDictionary *resultData = [data objectForKey:@"result" ];
//    
//    Profile *userProfile = [[[Profile alloc]init] autorelease];
//    [userProfile mapToDictionary:[resultData objectForKey:@"profile"]];
//    
//    NSArray *streamData = [resultData objectForKey:@"stream"];
//    NSMutableArray *streamModel = [DataModelParser parseStreamWithJSONArray:streamData];
//    
//    return [[[NSDictionary alloc]
//             initWithObjects:[NSArray arrayWithObjects: userProfile, streamModel, nil]
//             forKeys:[NSArray arrayWithObjects: kProfileKey, kStreamKey, nil]]
//            autorelease];
//}
//
//
//+(NSMutableArray*)parseFollowJSON:(NSString*)json
//{
//    
//    NSDictionary *parsedData = [json objectFromJSONString];
//    NSArray *resultsData = [parsedData objectForKey:@"results"];
//    
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *data in resultsData)
//    {
//        User *user = [[User alloc]init];
//        [user mapToDictionary:data];
//        [model addObject:user];
//        [user release];
//    }
//    
//    return [model autorelease];
//    
//}
//
//+(void)parseInformationJSON:(NSString *)json forEstablishemnt:(Establishment*)establishment
//{
//    NSDictionary *data = [json objectFromJSONString];
//    NSDictionary *resultsData = [data objectForKey:@"result" ];
//    establishment.information = [resultsData objectForKey:@"information"];
//}
//
//+(NSMutableArray*)parseReviewJSON:(NSString*)json
//{
//    NSDictionary *parsedData = [json objectFromJSONString];
//    NSArray *resultsData = [parsedData objectForKey:@"results"];
//    
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *dataItem in resultsData)
//    {
//        Review *review = [[Review alloc] init];
//        [review mapToDictionary:dataItem];
//        [model addObject:review];
//        [review release];
//    }
//    return [model autorelease];
//}
//
//+(NSDictionary*)parseMyPlacesJSON:(NSString *)json
//{
//    NSDictionary *allData = [json objectFromJSONString];
//    NSDictionary *resultsData = [allData objectForKey:@"results" ];
//    NSArray *subscriptionsData = [resultsData objectForKey:@"subscriptions"];
//    
//    NSMutableArray *header = [[[NSMutableArray alloc]init] autorelease];
//    NSMutableArray *model = [[[NSMutableArray alloc]init] autorelease];
//    
//    for (NSDictionary *data in subscriptionsData)
//    {
//        NSDictionary *sectorData = [data objectForKey:@"sector" ];
//        Sector *sector = [[Sector alloc]init];
//        [sector mapToDictionary:sectorData];
//        [header addObject:sector];
//        [sector release];
//        
//        
//        NSArray *establishmentList = [data objectForKey:@"establishments"];
//        NSMutableArray *establishmentModel = [[NSMutableArray alloc]init];
//        for (NSDictionary *estData in establishmentList)
//        {
//            Establishment *establishment = [[Establishment alloc]init];
//            [establishment mapToDictionary:estData];
//            [establishmentModel addObject:establishment];
//            [establishment release];
//        }
//        [model addObject:establishmentModel];
//        [establishmentModel release];
//    
//    }
//    
//    return [[[NSDictionary alloc]
//             initWithObjects:[NSArray arrayWithObjects: header, model, nil]
//             forKeys:[NSArray arrayWithObjects: kPlacesHeader, kPlacesModel, nil]]
//            autorelease];
//}
//
//+(NSDictionary*)parseSettingsJSON:(NSString*)json
//{
//    NSMutableArray *settings = [json objectFromJSONString];
//    NSDictionary *data = [settings objectAtIndex:0];
//    return data;
//}
//
//+(NSMutableArray*)parseProfileSearchJSON:(NSString*)json
//{
//    NSDictionary *parsedData = [json objectFromJSONString];
//    NSDictionary *resultsData = [parsedData objectForKey:@"result"];
//    NSArray *profilesData = [resultsData objectForKey:@"profiles"];
//    
//    NSMutableArray *model = [[NSMutableArray alloc]init];
//    for (NSDictionary *data in profilesData)
//    {
//        Profile *profile = [[Profile alloc]init];
//        [profile mapToDictionary:data];
//        [model addObject:profile];
//        [profile release];
//    }
//    
//    return [model autorelease];
//}
//
//+(NSDictionary*)parseReviewPostResultJSON:(NSString*)json
//{
//    return [json objectFromJSONString];
//}


@end
