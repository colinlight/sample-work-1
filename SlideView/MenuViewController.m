//
//  SettingsViewController.m
//  SlideView
//
//  Created by Colin Light on 07/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "MenuViewController.h"
#import "HomeViewController.h"
#import "DataModelParser.h"
#import "Setting.h"
#import "MenuCell.h"

#import "UITableViewCell+SettingsCellStyles.h"
#import "UISearchBar+SearchBarStyles.h"
#import "UITableView+SettingsTableStyles.h"


@interface MenuViewController ()
-(SettingCellStyle)getCellStyleForIndex:(int)index withDataModel:(NSMutableArray*)model;
@end

@implementation MenuViewController

@synthesize settingsTable;

@synthesize searchBar;

@synthesize tabDelegate;

@synthesize settings;


static int NUMBER_OF_SECTIONS = 4;

static int PROFILE_IMAGE = 0;

static int SETTINGS = 1;

static int ACCOUNT = 2;

//static int OPTIONS = 2;


#pragma mark - life cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        NSString *settingsPath = [[NSBundle mainBundle]pathForResource:@"settings" ofType:@"json"];
        NSString *jsonString = [[NSString alloc]initWithContentsOfFile:settingsPath
                                                              encoding:NSUTF8StringEncoding error:nil];
        
        self.settings = [DataModelParser parseSettingsJSON:jsonString];
        [jsonString release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.settingsTable setSettingsTableBackground];
    [self.searchBar setDefaultBarStyle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.settingsTable = nil;
    self.searchBar = nil;
    self.settings = nil;
}

-(void)dealloc
{
    [settingsTable release];
    [searchBar release];
    [settings release];
    [super dealloc];
}

#pragma mark - private methods
-(SettingCellStyle)getCellStyleForIndex:(int)index withDataModel:(NSMutableArray*)model
{
    if ( index == 0 )
        return SettingCellStyleTop;
    
    if ( index == ( [model count]-1 ) )
        return SettingCellStyleBottom;
    
    return SettingCellStyleMiddle;
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return NUMBER_OF_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( section == PROFILE_IMAGE )
        return  1;
    
    if ( section == SETTINGS )
        return [self.settings.sections count];
    
    if ( section == ACCOUNT )
        return [self.settings.account count];
    
    return [self.settings.options count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Setting *setting;
    
    if ( indexPath.section == PROFILE_IMAGE )
    {
        static NSString *UserCellIdentifier = @"UserCellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UserCellIdentifier];
        if ( !cell )
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:UserCellIdentifier];
            [cell setUserCellStyle];
        }
        cell.textLabel.text = @"User Name:";
        
        return cell;
    }
    
    
    if ( indexPath.section == SETTINGS )
    {
        static NSString *SettingsCellIdentifier = @"SettingsCellIdentifier";
        MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:SettingsCellIdentifier];
        if ( !cell )
        {
            cell = [[MenuCell alloc]initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:SettingsCellIdentifier
                                         withDivider:YES];
        }
        
        setting = [self.settings.sections objectAtIndex:indexPath.row];
        [cell setSetting:setting withStyle:[ self getCellStyleForIndex:indexPath.row
                                                         withDataModel:self.settings.sections]];
    
        return cell;
    }
    
    
    if ( indexPath.section == ACCOUNT )
    {
        static NSString *AccountCellIdentifier = @"AccountCellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AccountCellIdentifier];
        if ( !cell )
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:AccountCellIdentifier];
            [cell setSettingCellWithStyle:[ self getCellStyleForIndex:indexPath.row
                                                        withDataModel:self.settings.account]];
        }
        setting = [self.settings.account objectAtIndex:indexPath.row];
        cell.textLabel.text = setting.title;
        
        return cell;
    }

    
    static NSString *OptionsCellIdentifier = @"OptionsCellIdentifier";
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:OptionsCellIdentifier];
    if ( !cell )
    {
        cell = [[MenuCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                     reuseIdentifier:OptionsCellIdentifier withDivider:YES];
    }
    
    setting = [self.settings.options objectAtIndex:indexPath.row];
    [cell setSetting:setting withStyle:[ self getCellStyleForIndex:indexPath.row
                                                     withDataModel:self.settings.options]];
    
    return cell;
}


#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ( indexPath.section == SETTINGS )
    {
        int index = 0;
        
        if ( indexPath.row > 0 )
            index = 1;
        
        [self.tabDelegate selectedIndexChanged:index];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == PROFILE_IMAGE )
        return 45;
    
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ( section == ACCOUNT )
    {
        
        UIView *accountHeading = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 23)];
        UIImageView *background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"settings-table-header"]];
        background.frame = CGRectMake(10, 0, self.view.frame.size.width, 23);
        [accountHeading addSubview:background];
        [background release];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, self.view.frame.size.width, 23)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.shadowOffset = CGSizeMake(0, -1);
        titleLabel.font = [UIFont systemFontOfSize:12];
       // titleLabel.font = [UIFont getStandardCellSystemFont:14];
        //titleLabel.shadowColor = [UIColor colorWithRGBWhereRed:65 whereGreen:82 whereBlue:94 whereAlpha:1.0];
        titleLabel.text = @"ACCOUNT";
        
        [accountHeading addSubview:titleLabel];
        [titleLabel release];
        
        return [accountHeading autorelease];
        
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ( section == ACCOUNT )
        return 28;
    
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if ( section == SETTINGS )
        return 20;
    
    if ( section == ACCOUNT )
        return 12;
    
    return 10;
}





@end
