//
//  SlideViewController.m
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "SlideViewController.h"

@interface SlideViewController ()
-(void)centerViewForViewController:(UIViewController*)controller;
-(void)animateTopViewTo:(CGRect)newFrame isClosing:(BOOL)state;
-(void)openSlideView;
-(void)closeSlideView;
@end

@implementation SlideViewController

@synthesize topViewController;

@synthesize bottomViewController;


#pragma mark - life cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        mIsClosed = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    
    self.view.userInteractionEnabled = YES;
        
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(userDidPan:)];
    [self.view addGestureRecognizer:panGesture];
    [panGesture release];

}

-(void)viewDidUnload
{
    [super viewDidUnload];
    self.topViewController = nil;
    self.bottomViewController = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [topViewController release];
    [bottomViewController release];
    [super dealloc];
}

#pragma mark - public methods
-(void)setBottomViewController:(UIViewController *)newBottomViewController
{
    [newBottomViewController retain];
    [bottomViewController release];
    bottomViewController = newBottomViewController;
    
    [self centerViewForViewController:bottomViewController];
    
    [self.view addSubview:bottomViewController.view];
    [self.view sendSubviewToBack:bottomViewController.view];
}

-(void)setTopViewController:(UIViewController *)newTopViewController
{
    [newTopViewController retain];
    [topViewController release];
    topViewController = newTopViewController;
    
    [self centerViewForViewController:topViewController];
    
    [self.view addSubview:topViewController.view];
    [self.view bringSubviewToFront:topViewController.view];
}





-(IBAction)userDidPan:(UIPanGestureRecognizer*)sender
{
    CGPoint panPoint = [sender translationInView:self.view];
    
    if ( [sender state] == UIGestureRecognizerStateBegan )
        mStartPosition = self.topViewController.view.frame.origin;
    
    CGRect newFrame = [self.topViewController.view frame];
    newFrame.origin.x = mStartPosition.x + panPoint.x;
    
    if ( newFrame.origin.x < 0 )
        newFrame.origin.x = 0;
    
    self.topViewController.view.frame = newFrame;
    
    if ( [sender state] == UIGestureRecognizerStateEnded )
    {
        
        if ( newFrame.origin.x > (self.view.frame.size.width/3) )
        {
            [self openSlideView];
            return;
        }
        [self closeSlideView];
    }
}

#pragma mark - private methods
-(void)centerViewForViewController:(UIViewController*)controller
{
    CGRect currentRect = self.view.frame;
    currentRect.origin.y = 0;
    currentRect.origin.x = 0;
    currentRect.size.height = currentRect.size.height;
    controller.view.frame = currentRect;
}

-(void)animateTopViewTo:(CGRect)newFrame isClosing:(BOOL)state
{
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.topViewController.view.frame = newFrame;
                         mIsClosed = state;
        
    }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)toggleSlideView
{
    if ( mIsClosed )
    {
        [self openSlideView];
        return;
    }
    
    [self closeSlideView];
}


-(void)openSlideView
{
    CGRect currentTopRect = self.topViewController.view.frame;
    currentTopRect.origin.x = self.view.frame.size.width - 10;
    [self animateTopViewTo:currentTopRect isClosing:NO];
}

-(void)closeSlideView
{
    CGRect currentTopRect = self.topViewController.view.frame;
    currentTopRect.origin.x = 0;
    [self animateTopViewTo:currentTopRect isClosing:YES];
}



@end
