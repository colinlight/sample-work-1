//
//  ProfileExampleViewController.m
//  SlideView
//
//  Created by Colin Light on 07/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "ProfileExampleViewController.h"

@interface ProfileExampleViewController ()

@end

@implementation ProfileExampleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
