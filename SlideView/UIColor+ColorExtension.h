//
//  UIColor+ColorExtension.h
//  RateMyKebab
//
//  Created by Colin Light on 12/07/2012.
//  Copyright (c) 2012 LightInteractive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorExtension)

+(UIColor*)colorWithRGBWhereRed:(float)red whereGreen:(float)green whereBlue:(float)blue whereAlpha:(float)alpha;

+(UIColor*)defaultOrangeColour;

+(UIColor*)defaultGreyColour;


@end
