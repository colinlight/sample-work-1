//
//  Settings.h
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject
{
    NSMutableArray *sections;
    NSMutableArray *account;
    NSMutableArray *options;
}

@property (nonatomic, retain ) NSMutableArray *sections;

@property (nonatomic, retain ) NSMutableArray *account;

@property (nonatomic, retain ) NSMutableArray *options;

@end
