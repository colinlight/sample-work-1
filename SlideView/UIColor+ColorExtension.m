//
//  UIColor+ColorExtension.m
//  RateMyKebab
//
//  Created by Colin Light on 12/07/2012.
//  Copyright (c) 2012 LightInteractive. All rights reserved.
//

#import "UIColor+ColorExtension.h"

@implementation UIColor (ColorExtension)

+(UIColor*)colorWithRGBWhereRed:(float)red
                     whereGreen:(float)green
                      whereBlue:(float)blue
                     whereAlpha:(float)alpha
{
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

+(UIColor*)defaultOrangeColour
{
    return [ UIColor colorWithRGBWhereRed:255 whereGreen:84 whereBlue:13 whereAlpha:1];
}

+(UIColor*)defaultGreyColour
{
    return [UIColor colorWithRGBWhereRed:143 whereGreen:143 whereBlue:143 whereAlpha:1.0];
}

@end
