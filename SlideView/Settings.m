//
//  Settings.m
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "Settings.h"

@implementation Settings

@synthesize sections;

@synthesize account;

@synthesize options;

-(void)dealloc
{
    [sections release];
    [account release];
    [options release];
    [super dealloc];
}

@end
