//
//  BaseDataVO.h
//  owzit
//
//  Created by Colin Light on 28/09/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseDataVO : NSObject

-(void)mapToDictionary:(NSDictionary*)dict;

@end
