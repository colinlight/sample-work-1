//
//  AppDelegate.m
//  SlideView
//
//  Created by Colin Light on 06/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "ProfileExampleViewController.h"


@implementation AppDelegate

static int HOME_VIEW = 0;


- (void)dealloc
{
    [_window release];
    [_slideViewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    SlideViewController *slideController = [[SlideViewController alloc]init];
    self.slideViewController = slideController;
    [slideController release];
    
    
    
    CustomTabController *tabBarController = [[CustomTabController alloc] initWithNumberOfTabs:1];
    tabBarController.delegate = self;
    self.slideViewController.topViewController = tabBarController;
    
    
    
    MenuViewController *settingsController = [[MenuViewController alloc]
                                                  initWithNibName:@"MenuViewController" bundle:nil];
    settingsController.view.frame = self.window.frame;
    self.slideViewController.bottomViewController = settingsController;
    settingsController.tabDelegate = tabBarController;
    
    
    
    
    [settingsController release];
    [tabBarController release];
    
    self.window.rootViewController = self.slideViewController;
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - CustomTabControllerDelegate methods
-(void)setNavItemTitleForSection:(UINavigationItem*)item
{
}

-(UIViewController*)getViewControllerForIndex:(int)index
                           withNavigationItem:(UINavigationItem*)item
{
    if ( index == HOME_VIEW )
    {
        HomeViewController *homeViewController = [[[HomeViewController alloc]
                                                  initWithNibName:@"HomeViewController" bundle:nil] autorelease];
        homeViewController.slideDelegate = self.slideViewController;
        return homeViewController;
    }
    
    ProfileExampleViewController *profileView = [[[ProfileExampleViewController alloc]
                                                 initWithNibName:@"ProfileExampleViewController" bundle:nil]autorelease];
    
    return profileView;
}

@end
