//
//  Setting.h
//  SlideView
//
//  Created by Colin Light on 08/02/2013.
//  Copyright (c) 2013 Colin Light. All rights reserved.
//

#import "BaseDataVO.h"

@interface Setting : BaseDataVO
{
    NSString *title;
    NSString *icon;
    NSString *subtitle;
}

@property ( nonatomic, retain )  NSString *title;

@property ( nonatomic, retain )  NSString *icon;

@property ( nonatomic, retain )  NSString *subtitle;


@end
