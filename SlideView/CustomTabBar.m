//
//  CustomTabBar.m
//  owzit
//
//  Created by Colin Light on 23/10/2012.
//  Copyright (c) 2012 demomedia. All rights reserved.
//

#import "CustomTabBar.h"
#import "UIColor+ColorExtension.h"
#import "UIFont+FontExtensions.h"

@implementation CustomTabBar

@synthesize tabs;

@synthesize currentIndex;

@synthesize delegate;


#pragma mark - life cycle
- (id)initWithFrame:(CGRect)frame withNumberOfTabs:(int)number
{
    self = [super initWithFrame:frame];
    if (self) {
        NSMutableArray *allTabs = [[NSMutableArray alloc]init];
        for (int i = 0; i < number; i++) {
            
            UIButton *tabButton = [[UIButton alloc]init];
            [tabButton.titleLabel setFont: [UIFont getDefaultCellFontForSize:14]];
            [self addSubview:tabButton];
            [allTabs addObject:tabButton];
            [tabButton release];
            
            [tabButton addTarget:self action:@selector(didSelectTab:)
                forControlEvents:UIControlEventTouchDown];
        }
        self.tabs = allTabs;
        [allTabs release];
        
        UIButton *button = [self.tabs objectAtIndex:0];
        [button setSelected:YES];

    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGRect tabRect = self.frame;
    tabRect.size.width = tabRect.size.width/[self.tabs count];
    tabRect.origin.x = 0;
    for (UIButton *tabButton in self.tabs) {
        tabButton.frame = tabRect;
        tabRect.origin.x += tabRect.size.width;
    }

}

-(void)dealloc
{
    [tabs release];
    [super dealloc];
}

#pragma mark - public methods
-(void)setCurrentIndex:(int)ind
{
    if ( ind == currentIndex )
        return;
    
    UIButton *button = [self.tabs objectAtIndex:currentIndex];
    [button setSelected:NO];
    currentIndex = ind;
    
    UIButton *selectedbutton = [self.tabs objectAtIndex:currentIndex];
    [selectedbutton setSelected:YES];
    
    [self.delegate selectedIndexChanged:currentIndex];

}

-(void)setTabSkinAtIndex:(int)index
        withDefaultImage:(UIImage*)defaultImg
       withSelectedImage:(UIImage*)selectedImg
{
    UIButton *button = [self.tabs objectAtIndex:index];
    [button setBackgroundImage:defaultImg forState:UIControlStateNormal];
    [button setBackgroundImage:selectedImg forState:UIControlStateSelected];
}

-(void)setTabTitleAtIndex:(int)index withString:(NSString*)tabLabel
{
    UIButton *button = [self.tabs objectAtIndex:index];
    
    if ( !button )
        return;
    
    [button setTitle:tabLabel forState:UIControlStateNormal];
    [button setTitleColor:[UIColor
                           colorWithRGBWhereRed:143 whereGreen:143 whereBlue:143 whereAlpha:1.0]
                 forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
}

-(void)setTabIconAtIndex:(int)index
           withIconImage:(UIImage*)iconImg
       withSelectedImage:(UIImage*)selectedIcon
{
    UIButton *button = [self.tabs objectAtIndex:index];
    [button setImage:iconImg forState:UIControlStateNormal];
    [button setImage:selectedIcon forState:UIControlStateSelected];

    [button setTitleEdgeInsets:UIEdgeInsetsMake(42, -iconImg.size.width, 0, 0)];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    int buttonWidth = mainFrame.size.width / [self.tabs count];
    
    int imageXoffset = buttonWidth/2 - iconImg.size.width/2;
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, imageXoffset, 14, imageXoffset)];
}

-(IBAction)didSelectTab:(id)sender
{
    int selected = [self.tabs indexOfObject:sender];
    [self setCurrentIndex:selected];
}

@end
